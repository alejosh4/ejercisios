using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour
{
    public float walkSpeed = 5f;
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("Capsule").gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        movement();
    }
    private void movement(){
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");
        Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        transform.Translate(movementDirection* (walkSpeed* Time.deltaTime));
        }
}
