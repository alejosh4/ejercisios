﻿using System;

namespace Actividad3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Ejercisio 1
            int x = 0;
            int y = 0;
            Console.WriteLine("Introduzca un numero");
            x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduzca otro numero");
            y = Convert.ToInt32(Console.ReadLine());
            if(x > 0 && y > 0)
            {
                Console.WriteLine("Ambos numeros son positivos");
            }else if(x < 0 && y < 0)
            {
                Console.WriteLine("Ambos numeros son negativos");
            }
            else
            {
                Console.WriteLine("Solo un numero es positivo, el otro siendo negativo");
            }

            //Ejercisio 2
            long x = 0;
            long y = 0;
            Console.WriteLine("Introduzca un numero");
            x = Convert.ToInt64(Console.ReadLine());
            Console.WriteLine("Introduzca otro numero");
            y = Convert.ToInt64(Console.ReadLine());
            if (x % y == 0)
            {
                Console.WriteLine("{0} es multiplo de {1}", x, y);
            }
            else if (y % x == 0)
            {
                Console.WriteLine("{0} es multiplo de {1}", y, x);
            }
            else
            {
                Console.WriteLine("Ambosnumeros no son multiplos de entre ellos");
            }

            //Ejercisio 3
            int x = 10;
            int count = 1;
            Console.WriteLine("Introduzca un numero");
            long num = Convert.ToInt64(Console.ReadLine());
            while (x <= num)
            {
                x *= 10;
                count++;
            }
            Console.WriteLine("El numero tiene {0} digitos", count);

            //Ejercisio 4
            int guess;
            int count = 3;
            //La respuesta es 2
            while(count > 0)
            {
                Console.WriteLine("Adivina un numero del 1 al 10, tienes {0} intentos", count);
                guess = Convert.ToInt32(Console.ReadLine());
                if(guess == 2)
                {
                    Console.WriteLine("CORRECTO");
                    break;
                }else if (guess > 10 || guess < 1)
                {
                    Console.WriteLine("Ese no es un numero valido");
                }
                else
                {
                    Console.WriteLine("Incorrecto");
                    count--; 
                }
            }

            //Ejercisio 5
            int sum = 0;
            byte x;
            do
            {
                Console.WriteLine("Introduzca un numero, 0 para terminar");
                x = Convert.ToByte(Console.ReadLine());
                sum += x;
            } while (x != 0);
            Console.WriteLine("La suma de los numeros introducidos fue de {0}", sum);

            //Ejercisio 6
            int x;
            for (x = 0; x <= 9; x++)
            {
                Console.WriteLine("9 x {0} = {1}", x, x * 9);
            }

            //Ejercisio 7
            int mcd = 0;
            int i;
            Console.WriteLine("Introduzca un numero");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduzca otro numero");
            int y = Convert.ToInt32(Console.ReadLine());
            int low = x > y ? y : x;
            for (i = 2; i <= low; i++)
            {
                if(x % i == 0 && y % i == 0)
                {
                    mcd = i;
                }
            }
            Console.WriteLine("El MCD de {0} y {1} es {2}", x, y, mcd);

            //Ejercisio 8
            int guess;
            int count = 6;
            //La respuesta es 69 lo siento tenia que hacerlo
            while (count > 0)
            {
                Console.WriteLine("Adivina un numero del 1 al 100, tienes {0} intentos", count);
                guess = Convert.ToInt32(Console.ReadLine());
                if (guess == 69)
                {
                    Console.WriteLine("CORRECTO");
                    break;
                }
                else if (guess > 100 || guess < 1)
                {
                    Console.WriteLine("Ese no es un numero valido");
                }
                else
                {
                    Console.WriteLine("Incorrecto");
                    count--;
                    if(guess < 69)
                    {
                        Console.WriteLine("El numero debe ser mas alto");
                    }
                    else
                    {
                        Console.WriteLine("El numero debe ser mas bajo");
                    }
                }
            }

            //Ejercisio 9
            int i;
            Console.WriteLine("Introduzca el numero base");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduzca el numero exponente");
            int y = Convert.ToInt32(Console.ReadLine());
            int sum = x;
            for (i = 1; i < y; i++)
            {
                sum *= x;
            }
            Console.WriteLine("El resultado es {0}", sum);

            //Ejercisio 10
            Console.WriteLine("Introduzca el alto y ancho");
            int size = Convert.ToInt32(Console.ReadLine());
            int x, y;
            for (x=size;x>0;x--)
            {
                for (y = 0; y < x; y++)
                {
                    Console.Write("@");
                }
                Console.Write("\n");
            }

            //Ejercisio 11
            Console.WriteLine("Introduzca el alto");
            int tall = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduzca el ancho");
            int fat = Convert.ToInt32(Console.ReadLine());
            int x, y;
            for (x = 1; x <= tall; x++)
            {
                for (y = 1; y <= fat; y++)
                {
                    if (y == 1 || y == fat)
                    {
                        Console.Write("@");
                    }
                    else if (x == 1 || x == tall)
                    {
                        Console.Write("@");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.Write("\n");
            }

            //Ejercisio 12
            Console.WriteLine("Introduzca el precio");
            int price = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduzca el pago");
            int pay = Convert.ToInt32(Console.ReadLine());
            int total = pay - price;
            int two=0, five=0, ten=0, xx=0, vx=0, m = 0;
            if (total < 0)
            {
                Console.WriteLine("No se pago lo suficiente");
            }
            else
            {
                while(total >= 100)
                {
                    m++;
                    total -= 100;
                }
                while (total >= 50)
                {
                    vx++;
                    total -= 50;
                }
                while (total >= 20)
                {
                    xx++;
                    total -= 20;
                }
                while (total >= 10)
                {
                    ten++;
                    total -= 10;
                }
                while (total >= 5)
                {
                    five++;
                    total -= 5;
                }
                while (total >=2)
                {
                    two++;
                    total -= 2;
                }
                Console.WriteLine("100 = {0}\n50 = {1}\n20 = {2}\n10 = {3}\n5 = {4}\n2 = {5}\n", m, vx, xx, ten, five, two);
            }
        }
    }
}
